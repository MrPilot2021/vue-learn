var app = new Vue({
    el:"#app",
    data:{
        city: "北京",
        cityResult:[]
    },
    methods:{
        search: function(){
            // 注意作用域
            var that = this;
            axios.get("http://ajax-api.itheima.net/api/weather?city="+this.city)
            .then(function(response){
                that.cityResult = response.data.data.data;
            },function(err){
                console.log(err);
            })
        },
        useNewCity: function(city){
            this.city = city;
            this.search();
        }
    }
});